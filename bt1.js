 /*Mặc định cho 1 tháng có 30 ngày*/
function nhapKetqua(){
    var ngay=document.getElementById("ngay").value;
    var thang=document.getElementById("thang").value;
    var nam=document.getElementById("nam").value;
    ngayThangNam=null;
    if(ngay>30 || ngay < 1 || thang > 12 || thang <1){
        alert("Giá trị nhập ngày tháng năm sai");
    }else{
        if(ngay < 30 && ngay > 1){
            homNay=`Hôm nay: ${ngay} tháng ${thang} năm ${nam}`;
            homQua=`Hôm qua: ${ngay *1 -1} tháng ${thang} năm ${nam}`;
            homToi=`Hôm mai: ${ngay *1 +1} tháng ${thang} năm ${nam}`;

        }else if(ngay == 30 && thang < 12){
            homNay=`Hôm nay: ${ngay} tháng ${thang} năm ${nam} `;
            homQua=`Hôm qua: ${ngay} tháng ${thang *1-1} năm ${nam}`;
            homToi=`Hôm mai: ${ngay} tháng ${thang*1+1} năm ${nam}`;
        }else if(ngay == 1 && thang==1 ){
            homNay=`Hôm nay: ${ngay} tháng ${thang} năm ${nam}`;
            homQua=`Hôm qua: 30 tháng 12 năm ${nam *1 -1 }`;
            homToi=`Hôm mai: ${ngay*1 +1} tháng ${thang} năm ${nam}`;
        }else if(ngay==1 && thang >1){
            homNay=`Hôm nay: ${ngay} tháng ${thang} năm ${nam}`;
            homQua=`Hôm qua: 30 tháng ${thang*1 -1} năm ${nam}`;
            homToi=`Hôm mai: ${ngay*1 +1} tháng ${thang} năm ${nam}`;
        }else if(ngay== 30 && thang==12){
            homNay=`Hôm nay: ${ngay} tháng ${thang} năm ${nam}`;
            homQua=`Hôm qua: ${ngay*1-1} ${thang} năm ${nam}`;
            homToi=`Hôm mai: 1 tháng 1 năm ${nam*1+1}`;

        }
        console.log(homNay,homQua,homToi);
        document.getElementById("hom_nay").innerHTML = homNay;
        document.getElementById("hom_sau").innerHTML = homQua;
        document.getElementById("hom_qua").innerHTML = homToi;
    }
       
}
  